def read_rgb_matrix_and_normalize(filename):
    from PIL import Image
    import numpy as np
    pic = Image.open(filename)
    (x, y) = pic.size
    pixels = pic.load()
    output = np.zeros((x, y, 3))
    for i in range(x):
        for j in range(y):
            output[j, i] = [x / 255 for x in pixels[i, j]][0:3]
    return output


def get_rgb_channels(matrix):
    (x, y, z) = matrix.shape

    import itertools
    red = [matrix[i, j][0] for i, j in itertools.product(range(x), range(y))]
    green = [matrix[i, j][1] for i, j in itertools.product(range(x), range(y))]
    blue = [matrix[i, j][2] for i, j in itertools.product(range(x), range(y))]

    return red, green, blue


def calculate_deviance(red, green, blue):
    import numpy as np
    return np.std(red), np.std(green), np.std(blue)


def calculate_average(red, green, blue):
    import numpy as np
    return np.average(red), np.average(green), np.average(blue)


def calculate_std(data):
    from sklearn.preprocessing import StandardScaler
    scaler = StandardScaler()
    return scaler.fit_transform(data)


def task_rgb():
    from matplotlib import pyplot as pypl
    print("Task: RGB")
    red = []
    blue = []
    green = []
    for i in range(5):
        output = read_rgb_matrix_and_normalize('picture_{}.png'.format(i + 1))
        print(output)
        r1, g1, b1 = get_rgb_channels(output)
        r1_dev, g1_dev, b1_dev = calculate_deviance(r1, g1, b1)
        r1_avg, g1_avg, b1_avg = calculate_average(r1, g1, b1)
        red.append((r1_dev, r1_avg))
        green.append((g1_dev, g1_avg))
        blue.append((b1_dev, b1_avg))
        print("RGB average for P{} is {} {} {}".format(i, r1_avg, g1_avg, b1_avg))
        print("RGB deviance for P{} is {} {} {}".format(i, r1_dev, g1_dev, b1_dev))
        pypl.subplot(231+i)
        pypl.imshow(output, extent=(0, 32, 0, 32))
    print("RED: {}".format(red))
    print("GREEN: {}".format(green))
    print("BLUE: {}".format(blue))
    std_red = calculate_std(red)
    std_blue = calculate_std(blue)
    std_green = calculate_std(green)
    print("RED STD: {}".format(std_red))
    print("GREEN STD: {}".format(std_green))
    print("BLUE STD: {}".format(std_blue))
    pypl.show()


def task_website():
    import bs4
    import urllib.request
    from matplotlib import pyplot as pypl
    print("Task: WEBSITE")
    link = "http://www.snopes.com/facebook-ai-developed-own-language/"

    webpage = str(urllib.request.urlopen(link).read())
    site = bs4.BeautifulSoup(webpage, 'html.parser')

    site = site.find('html')

    for tag in site.find_all(['script', 'style']):
        tag.decompose()

    text = site.get_text()
    print("0. Raw text")
    print(text)

    # Clean escaped newline and tabs and whitespace <- they obscure data
    text = str(text).replace('\\n', '').replace('\\t', '')

    import re
    import string
    pattern = re.compile('[^A-Za-z]', re.UNICODE)
    text = re.sub(pattern, '', text)

    print("1. Cleaned newline and tab characters, filtered to letters only")
    print(text)

    text = text.lower()
    print("2. Lowercase")
    print(text)

    print("3. Use histogram")
    pypl.hist(list(text), bins=len(string.ascii_letters))
    pypl.show()


def main():
    task_rgb()
    # task_sound()
    # task_website()


if __name__ == '__main__':
    import sys

    sys.exit(not main())
